# Genera la siguiente matriz de N*N donde N es impar:
#    1   6   10  13  15
#        2   7   11  14
#            3   8   12
#                4   9
#                    5

n = int(input("Ingrese N: "))

array = []

for i in range(n):
    array.append([])
    incremento = n
    for j in range(n):
        array[i].append(" ")
        if ( i == j):
            array[i][j] = i + 1
        elif ( i < j ):
            array[i][j] = array[i][j-1] + incremento
            incremento-=1

		
for i in range(n):
	acumulador = ""
	for j in range(n):
		acumulador = acumulador + "   " + str(array[i][j])
	print(acumulador)
