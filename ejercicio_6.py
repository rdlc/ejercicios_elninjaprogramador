# Una persona adquirio un producto para pagar en x meses. El primer mes pagó $10.000,
# el segundo $20.000, el tercero $40.000 y así sucesivamente. Realizar un programa
# en Python para determinar cuanto debe pagar en cada mes y total de lo pago despues
# de los x meses.

mensualidad = 10000
total = 0

x = int(input("Ingrese la cantidad de meses: "))

for i in range(x):
    print("El pago de la mensualidad " + str(i+1) + " es: " + str(mensualidad))
    total += mensualidad
    mensualidad *= 2

print("El pago total es de: " + str(total))