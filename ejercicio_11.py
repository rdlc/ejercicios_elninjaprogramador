# Capturar calificaciones de un grupo de 10 alumnos.
# Cada alumno tiene un numero de control que va del 1 al 10.
# Su programa debe pedir el nombre y la calificacion
# del alumno y almacenar la calificacion en su respectiva
# posicion de un arreglo (el numero de control). 
# Finalmente cuando solicite imprimir
# la calificacion de un alumno debe pedir el numero de control e 
# imprimir el nombre y la calificación.

array = []

for i in range(0, 4):
    name = input("Ingrese el nombre del alumno numero " + str(i+1) + ": ")
    grade = input("Ingrese la nota del alumno " + str(i+1) + ": ")
    array.append(
        {
            'name': name,
            'grade': grade
        }
    )

while(True):
    control = int(input("Ingrese el numero de control: "))
    print("El nombre es: " + array[control - 1]['name'])
    print("La nota es: " + array[control - 1]['grade'])
