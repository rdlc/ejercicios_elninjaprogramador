select *
into #temporal
from
(
select '1114' ccuenta, 10 nsaldo, '2021-01-10' dFecha union all
select '1114', 20, '2021-01-11' union all
select '1114', 80, '2021-01-12' union all
select '1114', 50, '2021-01-13' union all
select '1114', 70, '2021-01-14' union all
select '2810', 50, '2021-01-09' union all
select '2810', 70, '2021-02-09' union all
select '2810', 50, '2021-03-09'
)x

select *, T1.nsaldo - T.nsaldo AS Diferencia
from #temporal T1
JOIN #temporal T ON T.dFecha = DATEADD(DAY, 1, T1.dFecha)
where T1.ccuenta = '1114'

select 
t1.ccuenta Cuenta,
t1.dFecha DiaAnterior,
t.dFecha DiaSiguiente,
t1.nsaldo SaldoDiaAnterior,
t.nsaldo SaldoDiaSiguiente,
t1.nsaldo - t.nsaldo Diferencia
from #temporal T1
JOIN #temporal T ON T.dFecha = DATEADD(DAY, 1, T1.dFecha)
where T1.ccuenta = '1114'

