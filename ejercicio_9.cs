using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejercicio_8
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i < 101; i++)
            {
                string message = "";

                if ( !Convert.ToBoolean(i % 3) )
                {
                    message += "Tik";
                }
                if (!Convert.ToBoolean(i % 5))
                {
                    message += "Tok";
                }
                message = String.IsNullOrEmpty(message) ? i.ToString() : message;
                Console.WriteLine(message);
            }
            Console.ReadKey();
        }
    }
}
