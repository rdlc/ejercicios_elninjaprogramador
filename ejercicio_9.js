// ESCRIBIR UN PROGRAMA QUE IMPRIMA LOS NUMEROS DEL 1 AL 100,
// CON LA CONDICION QUE SI EL NUMERO A IMPRIMIR ES MULTIPLO DE 
// 3, IMPRIMA EL STRING "TIK", SI EL NUMERO ES MULTIPLO DE 5
// IMPRIMA "TOK", Y SI ES MULTIPLO DE AMBOS IMPRIMA TIKTOK

for (let i = 1; i < 101; i++) {
    let message = "";
    
    if(!(i%3)){
        message += "Tik"
    }
    if(!(i%5)){
        message += "Tok"
    }
    console.log(message || i)
}
