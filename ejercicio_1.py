# Calcular el tiempo entre 2 años
# Problema: Se desea saber cuántos meses han transcurrido
# entre años cualquiera dados.
# Datos de entrada: se requiere tener el valor del año mayor (x) y el año menor (y) 
# Datos de salida: hay que hallar la cantidad de meses transcurridos (z) entre esos dos años. 

x = int(input("Ingrese el año mayor: ")) # El año mayor
y = int(input("Ingrese el año menor: ")) # El año menor

if(y>x):
    print("El año menor supera al año mayor.")
else:
    z = (x-y) * 12
    print("La diferencia de meses entre los años ingresados son de: " + str(z) + " meses.")
