# Problema: Se requiere llenar un tanque que tiene una capacidad
# de 50 metros cúbicos. Haga un algoritmo que imprima las horas que tarda en 
# llenarse dicho tanque con una manguera que tiene una capacidad de L litros 
# de agua por minuto.

# Datos de entrada: se requiere tener la capacidad en Litros por 
# minuto de la manguera (Litrosxmin) 

# Datos de salida: hay que hallar la cantidad de horas (Xhoras) 
# que el tanque tarda en llenarse. 

# 1 metro cubico = 1000 litros
# 50 metros cubicos = 50,000 litros

capacidad_total = 50000

capacidad_manguera = int(input("Capacidad total de manguera: "))

tiempo_minutos = capacidad_total / capacidad_manguera

tiempo_horas = tiempo_minutos / 60

print("El total de horas que tarda en llenarse el tanque es de: " + str(tiempo_horas) + " horas.")
