# ESCRIBIR UN PROGRAMA QUE IMPRIMA LOS NUMEROS DEL 1 AL 100,
# CON LA CONDICION QUE SI EL NUMERO A IMPRIMIR ES MULTIPLO DE 
# 3, IMPRIMA EL STRING "TIK", SI EL NUMERO ES MULTIPLO DE 5
# IMPRIMA "TOK", Y SI ES MULTIPLO DE AMBOS IMPRIMA TIKTOK

for i in range(1, 101):
    message = ""
    if not ( i%3 ): # 30 % 3 = !0 = 1 = True
        message+= "Tik"
    if not ( i%5 ): 
        message+= "Tok"
    print( message or i )

    # if ( i % 3 == 0 and i % 5 == 0 ):
    #     print("tiktok")
    # elif ( i % 3 == 0 ):
    #     print("tik")
    # elif ( i % 5 == 0 ):
    #     print("tok")
    

