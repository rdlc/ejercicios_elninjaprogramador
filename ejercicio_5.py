# Realizar un programa, que capture de usuario 10 valores 
# y encuentre el mayor, el menor y el promedio.

mayor, menor, total = 0, 0, 0

for i in range(10):
    numero = int(input("Ingrese el valor " + str(i+1) +": "))
    mayor = numero if numero > mayor else mayor
    menor = numero if numero < menor else menor
    total += numero

print("El numero mayor es: " + str(mayor))
print("El numero menor es: " + str(menor))
print("El promedio de los numeros es: " + str(total/10))
