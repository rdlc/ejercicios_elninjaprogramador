# Escriba un programa que permita ingresar su
# primer nombre y nos muestre el monograma 
# compuesto por: primera letra, ultima letra y posicion
# donde se encuentra la primera "a"

# Ejemplo:
# Input: Juan // Output: Jn2
# Input: Jose // Output: Je-1

nombre = input("Ingresa el nombre: ")

posicion_a = str(nombre.index("a") if "a" in nombre else "-1") # condicon : true ? false

resultado = nombre[0] + nombre[-1] + posicion_a

print(resultado)