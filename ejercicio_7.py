# Diseñe e implemente un algoritmo que reciba dos numeros enteros n, m y
# permita hallar la suma de la serie m + mm + mmm + ... n terminos. Por ejemplo,
# si n fuera 4 y m fuera 8, la serie sería 8 + 88 + 888 + 8888 = 9872

n = int(input("Ingrese N: "))
m = int(input("Ingrese M: "))
suma = 0

# for i in range(1, n+1):
#     suma += int(m*i) # "8" * 1 = int("8") = 8 // "8" * 2 int("88") = 88

a = 0
for i in range(0,n):
    a += m * (10**i) # 8 (0 + (8x10^0) )  88 (8 + (8x10^1))800+88 
    suma+=a

print("La suma de la serie es: " + str(suma))

